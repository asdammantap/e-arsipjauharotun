<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body>
 <div class="breadLine">            
            <div class="arrow"></div>
            <div class="adminControl active">
                <?php echo "Hallo,$_SESSION[username]"; ?>
            </div>
        </div>
        
        <div class="admin">
            <div class="image">
                <img src="img/users/aqvatarius_s.jpg" class="img-polaroid"/>                
            </div>
            <ul class="control"> 
                <li><span class="icon-user"></span> <?php echo "Hallo, $_SESSION[username]"; ?></li>               
                <li><span class="icon-share-alt"></span> <a href="logout.php">Logout</a></li>

            </ul>
        </div>
        <div class="dr"><span></span></div>
        <div class="widget">
            <div class="input-append">
                <input id="appendedInputButton" style="width: 118px;" type="text"><button class="btn" type="button">Search</button>
            </div>             
        </div>
        <div class="dr"><span></span></div>
        <ul class="navigation">            
            <li class="active">
                <a href="?p=home">
                    <span class="isw-grid"></span><span class="text">Dashboard</span>
                </a>
            </li>
            <li class="openable">
                <a href="#">
                    <span class="isw-list"></span><span class="text">Olah Data Master</span>
                </a>
                <ul>
					<li>
                        <a href="?p=datapengguna">
                            <span class="icon-check"></span><span class="text">Pengguna</span>
                        </a>                  
                    </li>   
                    <li>
                        <a href="?p=datapegawai">
                            <span class="icon-check"></span><span class="text">Pegawai</span>
                        </a>                  
                    </li>   
                    <li>
                        <a href="?p=jenisarsip">
                            <span class="icon-check"></span><span class="text">Jenis Arsip</span>
                        </a>                  
                    </li>                     
                    <li>
                        <a href="?p=lokasiarsip">
                            <span class="icon-check"></span><span class="text">Lokasi Arsip</span>
                        </a>                  
                    </li>   
                </ul>                
            </li> 
			<li>
                <a href="?p=arsip">
                    <span class="isw-list"></span><span class="text">Kelola Data Arsip</span>
                </a>
            </li> 
			
			 <li class="openable">
                <a href="#">
                    <span class="isw-list"></span><span class="text">Laporan</span>
                </a>
                <ul>
					<li>
                        <a href="?p=laporanarsipall">
                            <span class="icon-check"></span><span class="text">Arsip Keseluruhan</span>
                        </a>                  
                    </li>   
                    <li>
                        <a href="?p=lembardisposisi">
                            <span class="icon-check"></span><span class="text">Lembar Disposisi</span>
                        </a>                  
                    </li>                      
                </ul>                
            </li>                                   
        </ul>
</body>
</html>